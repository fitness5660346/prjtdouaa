<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "todolist";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $title = isset($_POST['title']) ? $_POST['title'] : null;
    $action = isset($_POST['action']) ? $_POST['action'] : null;

    if ($action === 'new') {
        $stmt = $conn->prepare("INSERT INTO todo (title) VALUES (?)");
        $stmt->bind_param("s", $title);
        $stmt->execute();
    } elseif ($action === 'delete') {
        $stmt = $conn->prepare("DELETE FROM todo WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
    } elseif ($action === 'toggle') {
        $stmt = $conn->prepare("UPDATE todo SET done = 1-done WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
    }

    $stmt->close();
}

$stmt = $conn->prepare("SELECT * FROM todo ORDER BY created_at DESC");
$stmt->execute();
$result = $stmt->get_result();
$taches = $result->fetch_all(MYSQLI_ASSOC);
$stmt->close();

$conn->close();



?>